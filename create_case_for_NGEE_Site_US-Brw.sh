#!/bin/sh


SITE_ID=A_transect
CYYMMDD=c150330
RES_NAME=416x1pt
COMPSET_TYPE=I1850CLM45
MACH=userdefined
COMPILER=gnu

#
# Case related option
#
SRC_DIR=
DIN_LOC_ROOT=
CASE_DIR=.
CASE_NAME=
#
# Physics related options:
#
SNOW_REDIST_FLAG=false
PETSC_THERM_MODEL_FLAG=false
THERM_REDIST_FLAG=false
PETSC_UNSATFLOW_MODEL_FLAG=false
UNSATFLOW_REDIST_FLAG=false

#
# Compiler options
#
PETSC_DIR_LOC=
PETSC_ARCH_LOC=
MPICC_LOC=
MPICXX_LOC=
MPIFC_LOC=
MPIEXEC_LOC=
SCC_LOC=
SCXX_LOC=
SFC_LOC=

usage ()
{
  echo >&2 \
      "usage: $0 [options] \n\n"\
      "OPTIONS: \n"\
      "  Case-related options: \n"\
      "        -source_dir                      <dirpath>     Full path to the directory containing the source code (required)\n" \
      "        -inputdata_dir                   <dirpath>     Full path to the directory containing inputdata (required)\n" \
      "        -case_dir                        <dirpath>     Full path to the directory to put the new case (optional)\n" \
      "        -case_name                       <name>        Specifies the case name (optional)\n\n" \
      "  Physics-related options: \n"\
      "        -snow_redistribution             <true|false>  Specifies if snow-redistribution algorithm is turned on/off (optional)\n" \
      "        -petsc_thermal_model             <true|false>  Specifies if PETSc-based thremal model is turned on/off (optional)\n" \
      "        -thermal_redistribution          <true|false>  Specifies if subsurface thermal redistribution is turned on/off \n" \
      "                                                       Requires PETSc-based thermal to be turned on (optional)\n" \
      "        -petsc_unsaturated_flow_model    <true|false>  Specifies if PETSc-based subsurface flow model is turned on/off (optional)\n" \
      "        -unsaturated_flow_redistribution <true|false>  Specifies if subsurface flow redistribution is turned on/off\n" \
      "                                                       Requires PETSc-based subsurface flow model to be turned on. (optional)\n\n"\
      "  Compiler-related options: \n" \
      "        -petsc_dir                       <dirpath>     Path to PETSc directory (optional)\n" \
      "        -petsc_arch                      <dirpath>     The PETSC_ARCH variable used during configuration and build of PETSc(optional)\n" \
      "        -mpicc                           <path>        Parallel C compiler (optional)\n" \
      "        -mpicxx                          <path>        Parallel C++ compiler (optional)\n" \
      "        -mpifc                           <path>        Parallel Fortran compiler (optional)\n" \
      "        -mpiexec                         <path>        Path to MPI exec (optional)\n" \
      "        -scc                             <path>        Sequential C compiler (optional)\n" \
      "        -scxx                            <path>        Sequential C++ compiler (optional)\n" \
      "        -sfc                             <path>        Sequential Fortran compiler (optional)\n"
}


# Get command line arguments
while [ $# -gt 0 ]
do
  case "$1" in
    -source_dir                           ) SRC_DIR="$2"; shift;;
    -inputdata_dir                        ) DIN_LOC_ROOT="$2"; shift;;
    -case_dir                             ) CASE_DIR="$2"; shift;;
    -case_name                            ) CASE_NAME="$2"; shift;;
    -snow_redistribution                  ) SNOW_REDIST_FLAG="$2"; shift;;
    -petsc_thermal_model                  ) PETSC_THERM_MODEL_FLAG="$2"; shift;;
    -thermal_redistribution               ) THERM_REDIST_FLAG="$2"; shift;;
    -petsc_unsaturated_flow_model         ) PETSC_UNSATFLOW_MODEL_FLAG="$2"; shift;;
    -unsaturated_flow_redistribution      ) UNSATFLOW_REDIST_FLAG="$2"; shift;;
    -petsc_dir                            ) PETSC_DIR_LOC="$2"; shift;;
    -petsc_arch                           ) PETSC_ARCH_LOC="$2"; shift;;
    -mpicc                                ) MPICC_LOC="$2"; shift;;
    -mpifcxx                              ) MPICXX_LOC="$2"; shift;;
    -mpifc                                ) MPIFC_LOC="$2"; shift;;
    -mpiexec                              ) MPIEXEC_LOC="$2"; shift;;
    -scc                                  ) SCC_LOC="$2"; shift;;
    -scxx                                 ) SCXX_LOC="$2"; shift;;
    -sfc                                  ) SFC_LOC="$2"; shift;;
    -*) usage
      exit 1;;
    *)  break;;	# terminate while loop
  esac
  shift
done

#
# Check case-related options
#
case "$SRC_DIR" in
  "")
    echo "Invalid value for --source_dir."
    usage
    exit 1
    ;;
   *)
    if [ ! -d "$SRC_DIR" ]; then
      echo "Bailing out as the following source directory does not exist: " ${SRC_DIR}
      usage
      exit 1
    fi
esac

case "$DIN_LOC_ROOT" in
  "")
    echo "Invalid value for --inputdata_dir."
    usage
    exit 1
    ;;
   *)
    if [ ! -d "$DIN_LOC_ROOT" ]; then
      echo "Bailing out as the following inputdata directory does not exist: " ${SRC_DIR}
      usage
      exit 1
    fi
esac

case "$CASE_DIR" in
  ".")
    ;;
   *)
    if [ ! -d "$CASE_DIR" ]; then
      echo "Bailing out as the following case directory does not exist: " ${CASE_DIR} "\n"
      usage
      exit 1
    fi
esac

case "$CASE_NAME" in
  "")
    CASE_NAME=${SITE_ID}_${RES_NAME}_${COMPSET_TYPE}_US-Brw_case1
    ;;
  *)
    ;;
esac

#
# Check physics-related options
#
case "$SNOW_REDIST_FLAG" in
  "true" | "false") 
    ;;
  *)
    echo "Invalid value for --snow_redistribution."
    usage
    exit 1
    ;;
esac

case "$PETSC_THERM_MODEL_FLAG" in
  "true" | "false") 
    ;;
  *)
    echo "Invalid value for --petsc_thermal_model."
    exit 1
    ;;
esac

case "$THERM_REDIST_FLAG" in
  "true" | "false") 
    ;;
  *)
    echo "Invalid value for --thermal_redistribution."
    exit 1
    ;;
esac

case "$PETSC_UNSATFLOW_MODEL_FLAG" in
  "true" | "false") 
    ;;
  *)
    echo "Invalid value for --petsc_unsaturated_flow_model."
    exit 1
    ;;
esac

case "$UNSATFLOW_REDIST_FLAG" in
  "true" | "false") 
    ;;
  *)
    echo "Invalid value for --unsaturated_flow_redistribution."
    usage
    exit 1
    ;;
esac

#
# Check compiler-related options
#
case "$PETSC_DIR_LOC" in
  "")
    PETSC_DIR_LOC=`printenv PETSC_DIR`
    if [ "$PETSC_DIR_LOC" == "" ]; then
      echo "Could not find PETSc installation. "
      echo "PETSc installation directory is not specified via --petsc_dir and PETSC_DIR environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

if [ ! -d "$PETSC_DIR_LOC" ]; then
  echo "Bailing out as the following PETSc directory does not exist: " ${PETSC_DIR_LOC} "\n"
  usage
  exit 1
fi

case "$PETSC_ARCH_LOC" in
  "")
    PETSC_ARCH_LOC=`printenv PETSC_ARCH`
    if [ "$PETSC_ARCH" == "" ]; then
      echo "Could not find PETSC_ARCH installation. "
      echo "PETSC_ARCH is not specified via --petsc_arch and PETSC_ARCH environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

if [ ! -d "$PETSC_DIR_LOC/$PETSC_ARCH_LOC" ]; then
  echo "Bailing out as the following PETSc installation directory does not exist: " ${PETSC_DIR_LOC}/${PETSC_ARCH_LOC} "\n"
  usage
  exit 1
fi

case "$MPICC_LOC" in
  "")
    MPICC_LOC=`printenv MPICC`
    if [ "$MPICC_LOC" == "" ]; then
      echo "Could not find MPI C compiler. "
      echo "MPI C compiler is not specified via --mpicc and MPICC environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

case "$MPICXX_LOC" in
  "")
    MPICXX_LOC=`printenv MPICXX`
    if [ "$MPICXX_LOC" == "" ]; then
      echo "Could not find MPI C++ compiler. "
      echo "MPI C++ compiler is not specified via --mpicxx and MPICXX environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

case "$MPIFC_LOC" in
  "")
    MPIFC_LOC=`printenv MPIFC`
    if [ "$MPIFC_LOC" == "" ]; then
      echo "Could not find MPI Fortran compiler. "
      echo "MPI Fortran compiler is not specified via --mpifc and MPIFC environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

case "$MPIEXEC_LOC" in
  "")
    MPIEXEC_LOC=`printenv MPIEXEC`
    if [ "$MPIEXEC_LOC" == "" ]; then
      echo "Could not find MPI exec. "
      echo "MPI exec is not specified via --mpiexec and MPIEXEC environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

case "$SCC_LOC" in
  "")
    SCC_LOC=`printenv SCC`
    if [ "$SCC_LOC" == "" ]; then
      echo "Could not find a sequential C compiler. "
      echo "Sequential C compiler is not specified via --scc and SCC environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

case "$SCXX_LOC" in
  "")
    SCXX_LOC=`printenv SCXX`
    if [ "$SCXX_LOC" == "" ]; then
      echo "Could not find a sequential C++ compiler. "
      echo "Sequential C++ compiler is not specified via --scxx and SCXX environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

case "$SFC_LOC" in
  "")
    SFC_LOC=`printenv SFC`
    if [ "$SFC_LOC" == "" ]; then
      echo "Could not find a sequential Fortran compiler. "
      echo "Sequential Fortran compiler is not specified via --sfc and SFC environmental variable is undefined.\n"
      usage
      exit 1
    fi
    ;;
  *)
esac

echo ""
echo "Case options:"
echo "   Source direcotry           = $SRC_DIR"
echo "   Inputdata direcotry        = $DIN_LOC_ROOT"
echo "   Case direcotry             = $CASE_DIR"
echo "   Case name                  = $CASE_NAME"
echo "Physics options:"
echo "   Snow redistribution        = $SNOW_REDIST_FLAG"
echo "   PETSc thermal model        = $PETSC_THERM_MODEL_FLAG"
echo "   Thermal redistribution     = $THERM_REDIST_FLAG"
echo "   PETSc unsat. flow model    = $PETSC_UNSATFLOW_MODEL_FLAG"
echo "   Unsat. flow redistribution = $UNSATFLOW_REDIST_FLAG"
echo "Compiler options:"
echo "   PETSc directory              = $PETSC_DIR_LOC"
echo "   PETSC_ARCH             = $PETSC_ARCH_LOC"
echo "   MPICC                      = $MPICC_LOC"
echo "   MPICXX                     = $MPICXX_LOC"
echo "   MPIFC                      = $MPIFC_LOC"
echo "   MPIEXEC                    = $MPIEXEC_LOC"
echo "   SCC                        = $SCC_LOC"
echo "   SCXX                       = $SCXX_LOC"
echo "   SFC                        = $SFC_LOC"
echo ""

cd ${SRC_DIR}/scripts

# Creating case with command :
./create_newcase -case ${CASE_DIR}/${CASE_NAME} -res CLM_USRDAT -compset ${COMPSET_TYPE} -mach ${MACH} -compiler ${COMPILER}

# Configuring case :
cd ${CASE_DIR}/${CASE_NAME}
# Create Macros

cat > Macros <<EOF
include ${PETSC_DIR}/conf/variables

CPPDEFS+= -DFORTRANUNDERSCORE -DNO_R16 -DDarwin -DCPRGNU

SLIBS+=-L/opt/local/lib -lnetcdff -L/opt/local/lib -lnetcdf

CONFIG_ARGS:=

CXX_LINKER:=FORTRAN

ESMF_LIBDIR:=

FC_AUTO_R8:= -fdefault-real-8

FFLAGS:= -O -fconvert=big-endian -ffree-line-length-none -ffixed-line-length-none
FFLAGS+=-fno-range-check

FFLAGS_NOOPT:= -O0

FIXEDFLAGS:=  -ffixed-form

FREEFLAGS:= -ffree-form

MPICC:=${MPICC}

MPICXX:=${MPICXX}

MPIFC:=${MPIFC}

MPI_LIB_NAME:=

MPI_PATH:=

NETCDF_PATH:=/opt/local

PNETCDF_PATH:=

SCC:=${SCC}

SCXX:=${SCXX}

SFC:=${SFC}

SUPPORTS_CXX:=TRUE

# linking to external libraries...
USER_INCLDIR:=-I${PETSC_DIR}/${PETSC_ARCH}/include 
USER_INCLDIR +=-I${PETSC_DIR}/include 
FFLAGS       +=-DUSE_PETSC_LIB
LDFLAGS +=

ifeq (\$(DEBUG), TRUE)
   FFLAGS += -g -Wall
endif

ifeq (\$(compile_threaded), true)
   LDFLAGS += -fopenmp
   CFLAGS += -fopenmp
   FFLAGS += -fopenmp
endif

ifeq (\$(MODEL), cism)
   CMAKE_OPTS += -D CISM_GNU=ON
endif

ifeq (\$(MODEL), driver)
   LDFLAGS += -lnetcdff -lnetcdf -all_load
   LDFLAGS += -L${PETSC_DIR}/${PETSC_ARCH}/lib \$(PETSC_LIB)
   LDFLAGS += -framework Accelerate
endif

EOF



# Modifying : env_mach_pes.xml
./xmlchange -file env_mach_pes.xml -id NTASKS_ATM         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_LND         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_ICE         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_OCN         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_CPL         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_GLC         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_ROF         -val 1
./xmlchange -file env_mach_pes.xml -id NTASKS_WAV         -val 1
./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE -val 1
./xmlchange -file env_mach_pes.xml -id TOTALPES           -val 1

# Modifying : env_build.xml
./xmlchange -file env_build.xml -id GMAKE                 -val make
./xmlchange -file env_build.xml -id MPILIB                -val mpich
./xmlchange -file env_build.xml -id OS                    -val Darwin
./xmlchange -file env_build.xml -id COMPILER              -val gnu
./xmlchange -file env_build.xml -id DEBUG                 -val TRUE
./xmlchange -file env_build.xml -id EXEROOT               -val ${CASE_DIR}/${CASE_NAME}/bld
./xmlchange -file env_build.xml -id CESMSCRATCHROOT       -val ${CASE_DIR}/${CASE_NAME}

# Modifying : env_run.xml
./xmlchange -file env_run.xml -id DATM_MODE               -val CLM1PT
./xmlchange -file env_run.xml -id DATM_CLMNCEP_YR_START   -val 1981
./xmlchange -file env_run.xml -id DATM_CLMNCEP_YR_END     -val 2013
./xmlchange -file env_run.xml -id DATM_CLMNCEP_YR_ALIGN   -val 1
./xmlchange -file env_run.xml -id DIN_LOC_ROOT            -val ${DIN_LOC_ROOT}
./xmlchange -file env_run.xml -id DIN_LOC_ROOT_CLMFORC    -val ${DIN_LOC_ROOT}/atm/datm7
./xmlchange -file env_run.xml -id RUNDIR                  -val ${CASE_DIR}/${CASE_NAME}/run
./xmlchange -file env_run.xml -id CLM_USRDAT_NAME         -val 1x1pt_US-Brw_1981-2013
./xmlchange -file env_run.xml -id ATM_DOMAIN_FILE         -val ${SITE_ID}_domain.lnd.${RES_NAME}_US-Brw_navy_ugrid_${CYYMMDD}.nc
./xmlchange -file env_run.xml -id LND_DOMAIN_FILE         -val ${SITE_ID}_domain.lnd.${RES_NAME}_US-Brw_navy_ugrid_${CYYMMDD}.nc
./xmlchange -file env_run.xml -id LND_DOMAIN_PATH         -val ${DIN_LOC_ROOT}/share/domains
./xmlchange -file env_run.xml -id ATM_DOMAIN_PATH         -val ${DIN_LOC_ROOT}/share/domains

# Modify user_nl_clm

cat >> user_nl_clm << EOF
  hist_mfilt                  = 73
  hist_nhtfrq                 = -24
  use_snow_redistribution     = .$SNOW_REDIST_FLAG.
  shutoff_soilevap_below_tfrz = .true.
  use_petsc_thermal_model     = .$PETSC_THERM_MODEL_FLAG.
  use_thermal_redistribution  = .false.
  use_petsc_unsatflow_model   = .$PETSC_UNSATFLOW_MODEL_FLAG.
  more_vertlayers             = .$UNSATFLOW_REDIST_FLAG.
  fsurdat                     = '${DIN_LOC_ROOT}/lnd/clm2/surfdata_map/A_transect_surfdata_416x1pt_US-Brw_simyr1850_c150330.nc'
  flndtopo                    = '${DIN_LOC_ROOT}/lnd/clm2/surfdata_map/A_transect_surfdata_416x1pt_US-Brw_simyr1850_c150330.nc'
EOF

./cesm_setup

# Modify run script
perl -i -p -e 's@#mpiexec@'$MPIEXEC'@' ${CASE_NAME}.run

#perl -w -i -p -e 's@^sleep@#sleep@' ${CASE_NAME}.run

# Modify meteorological forcing data
cp ${CASE_DIR}/${CASE_NAME}/CaseDocs/datm.streams.txt.CLM1PT.CLM_USRDAT ${CASE_DIR}/${CASE_NAME}/user_datm.streams.txt.CLM1PT.CLM_USRDAT
chmod +w ${CASE_DIR}/${CASE_NAME}/user_datm.streams.txt.CLM1PT.CLM_USRDAT
chmod +r ${CASE_DIR}/${CASE_NAME}/user_datm.streams.txt.CLM1PT.CLM_USRDAT
perl -w -i -p -e "s@A_transect_domain.lnd.416x1pt_US-Brw_navy_ugrid_c150330.nc@domain.lnd.1x1pt_US-Brw_navy.nc@" ${CASE_DIR}/${CASE_NAME}/user_datm.streams.txt.CLM1PT.CLM_USRDAT


# building case :
./${CASE_NAME}.build

# Running case :
./${CASE_NAME}.run

