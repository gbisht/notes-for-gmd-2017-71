CDF       
      nj        ni        nv              
Created_by        Gautam Bisht (gbisht@lbl.gov)      
Created_on        Wed Aug 28 10:51:53 2013             area                   	long_name         $area of grid cell in radians squared   
coordinate        xc yc      units         radians2        �   frac                   	long_name         $fraction of grid cell that is active   
coordinate        xc yc      units         unitless   filter1       =error if frac> 1.0+eps or frac < 0.0-eps; eps = 0.1000000E-11      filter2       Jlimit frac to [fminval,fmaxval]; fminval= 0.1000000E-02 fmaxval=  1.000000          �   mask                   	long_name         land domain mask   
coordinate        xc yc      note      unitless   comment       70=ocean and 1=land, 0 indicates that cell is not active         �   xc                     	long_name         longitude of grid cell center      units         degrees_east   bounds        xv          �   xv                        	long_name         longitude of grid cell vertices    units         degrees_east         �   yc                     	long_name         latitude of grid cell center   units         degrees_north      bounds        yv          �   yv                        	long_name         latitude of grid cell vertices     units         degrees_north            �>ɍ���[�?�         @ik���-�@ij_��F@im�:)�z@ij_��F@im�:)�z@Q���
=q@Q�p��
>@Q�p��
>@Q��
=p�@Q��
=p�