## Description

For Bisht et al. (2017) study, this repository provides the following:

 - Meteorological driver dataset for a study site at the Barrow, Alaska Observatory.
   The was obtained by the [NGEE-Arctic project](https://ngee-arctic.ornl.gov).
 - A script to run the model for various configurations
   described in Bisht et al. (2017).

The model and user guide are available at [https://bitbucket.org/gbisht/lateral-subsurface-model](https://bitbucket.org/gbisht/lateral-subsurface-model).

## References

Bisht, G., Riley, W. J., Wainwright, H. M., Dafflon, B., Yuan, F., and Romanovsky, V. E.: Impacts of microtopographic snow redistribution and lateral subsurface processes on hydrologic and thermal states in an Arctic polygonal ground ecosystem: a case study using ELM-3D v1.0, Geosci. Model Dev., 11, 61-76, https://doi.org/10.5194/gmd-11-61-2018, 2018.
